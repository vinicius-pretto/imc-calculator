# IMC Calculator

## Running application

### Install dependencies

```
$ npm install
```

### Start application

```
$ npm start
```

### Run tests

```
$ npm test
```