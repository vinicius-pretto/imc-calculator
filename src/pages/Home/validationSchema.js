import * as yup from 'yup';

const validationSchema = yup.object().shape({
  name: yup
    .string()
    .min(3, 'O campo nome deve ter entre 3 e 80 caracters')
    .max(80, 'O campo nome deve ter entre 3 e 80 caracters')
    .required('O campo nome não pode ser vazio'),
  weight: yup
    .number()
    .required('O campo peso não pode ser vazio'),
  height: yup
    .number()
    .required('O campo altura não pode ser vazio')
});

export default validationSchema;
