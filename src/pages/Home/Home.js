import React from "react";
import styled from "styled-components";
import { Formik } from 'formik';

import Input from "../../components/Input";
import Button from '../../components/Button';
import ImcTable from '../../components/ImcTable';
import validationSchema from './validationSchema';
import Device from '../../styles/Device';

const Container = styled.div`
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  @media ${Device.TABLET} {
    height: auto;
    display: block;
    padding: 12px;
  }
`;

const Section = styled.section`
  width: 750px;
  display: block;

  @media ${Device.TABLET} {
    width: 100%;
  }
`;

const Title = styled.h1`
  font-size: ${props => props.theme.fonts.fontSizeLarge};
  font-weight: 500;
  color: ${props => props.theme.colors.text};

  @media ${Device.MOBILE} {
    font-size: 2rem;
  }
`;

const Form = styled.form`
  margin-top: 40px;
`

const FormGroup = styled.div`
  display: flex;

  @media ${Device.MOBILE} {
    flex-direction: column;
  }
`

const FormFooter = styled.div`
  display: flex;
  justify-content: flex-end;
`

const Message = styled.p`
  margin-top: 20px;
  color: ${props => props.theme.colors.text};
  font-size: ${props => props.theme.fonts.fontSizeSmall};
`

const Imc = styled.span`
  border-bottom: 5px solid;
  border-color: ${props => props.theme.colors.primary};
  color: ${props => props.theme.colors.text};
  padding: 5px;
  font-weight: bold;
`

const initialValues = {
  name: '',
  weight: '',
  height: ''
}

class Home extends React.Component {
  state = {
    imc: '',
    imcClassify: ''
  }

  classifyIMC = imc => {
    if (imc < 18.5) {
      return 'Magreza';
    } 
    if (imc >= 18.5 && imc <= 24.9) {
      return 'Normal';
    }
    if (imc > 24.9 && imc <= 30) {
      return 'Sobrepeso';
    }
    if (imc > 30) {
      return 'Obesidade';
    }
    return 'Não classificado';
  }

  calculateIMC = (weight, height) => {
    const imc = Number(weight) / (parseFloat(height) * parseFloat(height));
    return imc.toFixed(2);
  }

  onSubmit = values => {
    const imc = this.calculateIMC(values.weight, values.height);
    const imcClassify = this.classifyIMC(imc);
    this.setState({ imc, imcClassify });
  }

  render() {
    return (
      <Container>
        <Section>
          <Title>Calcule seu IMC</Title>
          <Formik
            initialValues={initialValues}
            onSubmit={this.onSubmit}
            validationSchema={validationSchema}
          >
            {props => {
              const {
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit
              } = props;

              return (
                <Form onSubmit={handleSubmit}>
                  <Input
                    id="name"
                    type="text"
                    label="Nome"
                    placeholder="Informe seu nome"
                    required
                    value={values.name}
                    error={errors.name}
                    touched={touched.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <FormGroup>
                    <Input
                      id="weight"
                      type="number"
                      label="Peso"
                      placeholder="Informe seu peso"
                      required
                      style={{ marginRight: '10px' }}
                      value={values.weight}
                      error={errors.weight}
                      touched={touched.weight}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <Input
                      id="height"
                      type="number"
                      label="Altura"
                      placeholder="Informe seu altura"
                      required
                      value={values.height}
                      error={errors.height}
                      touched={touched.height}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </FormGroup>
                  <FormFooter>
                    <Button id="submit-button" type="submit">Calcular</Button>
                  </FormFooter>
                  {this.state.imc &&
                    <Message id="message">
                      {`${values.name} seu IMC é: `}<Imc>{this.state.imc}</Imc>
                    </Message>
                  }
                </Form>
              )
            }}
          </Formik>
          <ImcTable imcClassify={this.state.imcClassify} />
        </Section>
      </Container>
    );
  }
};

export default Home;
