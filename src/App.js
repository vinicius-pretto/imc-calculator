import React from 'react';
import { ThemeProvider } from 'styled-components';

import GlobalStyle from './components/GlobalStyle';
import Home from './pages/Home';
import theme from './styles/theme';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <React.Fragment>
        <GlobalStyle />
        <Home />
      </React.Fragment>
    </ThemeProvider>
  )
}

export default App;
