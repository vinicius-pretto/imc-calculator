import React from 'react';
import styled from 'styled-components';

import Device from '../../styles/Device';

const imcData = [
  {
    id: 1,
    classification: 'Magreza',
    imc: '< 18.5',
    weight: '< 60.6 Kg'
  },
  {
    id: 2,
    classification: 'Normal',
    imc: '18.5 a 24.9',
    weight: '60.6 a 81.6 Kg'
  },
  {
    id: 3,
    classification: 'Sobrepeso',
    imc: '24.9 a 30',
    weight: '81.6 a 98.3 Kg'
  },
  {
    id: 4,
    classification: 'Obesidade',
    imc: '> 30',
    weight: '> 98.3 Kg'
  }
];

const Table = styled.table`
  width: 100%;
  border-collapse: collapse; 
  margin-top: 10%;
`

const TCell = styled.th`
  font-weight: normal;
  text-transform: uppercase;
  font-size: ${props => props.theme.fonts.fontSizeExtraSmall};
  background-color: ${props => props.theme.colors.text};
  color: #FFF;
  padding: 10px;
  text-align: left;
  
  @media ${Device.MOBILE} {
    font-size: .8rem;
  }
`

const Cell = styled.td`
  font-size: ${props => props.theme.fonts.fontSizeSmall};
  padding: 10px;
  border: 1px solid #ccc; 
  text-align: left; 

  @media ${Device.MOBILE} {
    font-size: .8rem;
  }
`

const Row = styled.tr`
  background: ${props => props.classified
    ? props.theme.colors.primary
    : 'transparent'
  };
  color: ${props => props.classified
    ? '#FFF'
    : props => props.theme.colors.text
  };
`

const ImcTable = ({ imcClassify }) => {
  return (
    <Table>
      <thead>
        <Row>
          <TCell>Classificação</TCell>
          <TCell>IMC</TCell>
          <TCell>Peso</TCell>
        </Row>
      </thead>
      <tbody>
        {imcData.map(data =>
          <Row key={data.id} classified={imcClassify === data.classification}>
            <Cell>{data.classification}</Cell>
            <Cell>{data.imc}</Cell>
            <Cell>{data.weight}</Cell>
          </Row>
        )}
      </tbody>
    </Table>
  )
}

export default ImcTable;
