import styled from 'styled-components';
import Device from '../../styles/Device';

const Button = styled.button`
  background-color: ${props => props.theme.colors.primary};
  border: 1px solid ${props => props.theme.colors.primary};
  border-radius: 4px;
  padding: 15px 25px;
  color: #FFF;
  font-size: ${props => props.theme.fonts.fontSizeSmall};
  cursor: pointer;

  @media ${Device.MOBILE} {
    width: 100%;
    font-size: 1rem;
  }
`

export default Button;
