import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    text-decoration: none;
    list-style: none;
  }
  
  body {
    font-family: ${props => props.theme.fonts.default};
    background: ${props => props.theme.colors.background};
  }
`;

export default GlobalStyle;
