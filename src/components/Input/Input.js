import React from "react";
import styled from "styled-components";

import ErrorMessage from "../ErrorMessage";
import Device from '../../styles/Device';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-bottom: 20px;
`;
Container.displayName = "Container";

const InputStyled = styled.input`
  height: 52px;
  border-width: 1px;
  border-color: ${props => props.theme.colors.text};
  border-style: solid;
  border-radius: 4px;
  padding-left: 10px;
  background: transparent;
  font-size: ${props => props.theme.fonts.fontSizeSmall};
  font-weight: 500;
  color: ${props => props.theme.colors.text};

  @media ${Device.MOBILE} {
    font-size: .9rem;
  }
`;
InputStyled.displayName = "Input";

const Label = styled.label`
  color: ${props => props.theme.colors.text};
  font-size: ${props => props.theme.fonts.fontSizeMedium};
  margin-bottom: 8px;

  @media ${Device.MOBILE} {
    font-size: 1rem;
  }
`;
Label.displayName = "Label";

const RequiredIcon = styled.span`
  color: ${props => props.theme.colors.error};
`
RequiredIcon.displayName = 'RequiredIcon';

const Input = props => {
  const { 
    id, 
    type, 
    label, 
    required, 
    placeholder, 
    style,
    value,
    error,
    touched,
    onChange,
    onBlur
  } = props;
  const hasError = error && touched;

  return (
    <Container style={style}>
      <Label name={id}>
        {label}
        {required && <RequiredIcon>*</RequiredIcon>}
      </Label>
      <InputStyled
        id={id}
        type={type}
        required={required}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
      />
      {hasError && <ErrorMessage>{error}</ErrorMessage>}
    </Container>
  );
};

export default Input;
