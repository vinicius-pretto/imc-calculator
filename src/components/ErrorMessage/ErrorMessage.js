import React from 'react';
import styled from 'styled-components';
import Device from '../../styles/Device';

const ErrorMessageStyled = styled.p`
  font-size: ${props => props.theme.fonts.fontExtraSmall};
  color: ${props => props.theme.colors.error};
  margin-top: 5px;

  @media ${Device.MOBILE} {
    font-size: .9rem;
  }
`

const ErrorMessage = ({ children }) => {
  return (
    <ErrorMessageStyled>{children}</ErrorMessageStyled>
  )
}

export default ErrorMessage;
