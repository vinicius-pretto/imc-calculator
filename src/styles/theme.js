const theme = {
  fonts: {
    default: 'Helvetica, Verdana, sans-serif',
    fontSizeLarge: '40px',
    fontSizeMedium: '20px',
    fontSizeSmall: '18px',
    fontExtraSmall: '13px'
  },
  colors: {
    primary: '#1334DE',
    error: '#E76666',
    text: '#333',
    background: '#F9F6F6'
  }
}

export default theme;
