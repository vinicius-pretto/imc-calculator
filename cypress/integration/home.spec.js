describe('Home', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('should fill the form and click on "Calcular" button', () => {
    cy.get('#name').type('Vinicius');
    cy.get('#weight').type('90');
    cy.get('#height').type('1.81');
    cy.get('#submit-button').click();
    cy.get('#message').contains('Vinicius seu IMC é: 27.47');
  });
});
